// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.Instant;

class ServerThread extends Thread {

    String command;
    BufferedReader inputStream;
    PrintWriter outputStream;
    Socket socket;

    CommandHandler cmdHandler;
    CommandTranslator trans;

    SimulatorConnector simulatorConnector;

    private Instant startingTS;
    private Instant endingTS;
    private long currentTickTime = 0;


    public ServerThread(Socket socket, SimulatorConnector simulatorConnector){
        cmdHandler = new CommandHandler();
        trans = new CommandTranslator();

        this.simulatorConnector = simulatorConnector;
        this.socket = socket;
    }

    public void run() {
        try {
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outputStream = new PrintWriter(socket.getOutputStream());
        } catch(IOException e) {
            e.printStackTrace();
        }

        try {

            boolean listeningState = true;

            while (listeningState) {

                command = inputStream.readLine();

                if (startingTS != null) {
                    endingTS = Instant.now();
                    currentTickTime = endingTS.toEpochMilli() - startingTS.toEpochMilli() ;
                }

                trans = cmdHandler.parseRequest(command);
                trans.setTickTime(currentTickTime);

                switch (trans.getCommand()) {

                    case INVALID_SYNTAX:
                        outputStream.println("Invalid Syntax");
                        break;

                    case TERMINATE:
                        simulatorConnector.sendMessage(trans.getSimulatorCommand());
                        outputStream.println(simulatorConnector.getSimulatorResponseXML());

                        this.closeCompetition();
                        listeningState = false;
                        break;

                    default:
                        if (!simulatorConnector.isConnected() || simulatorConnector.isCompetitionTerminated()) {
                            simulatorConnector.connect();
                        }
                        simulatorConnector.sendMessage(trans.getSimulatorCommand());

                        outputStream.println(simulatorConnector.getSimulatorResponseXML());

                        if (simulatorConnector.isCompetitionTerminated()) {
                            this.closeCompetition();
                            listeningState = false;
                        }

                        startingTS = Instant.now();

                        break;

                }

                outputStream.flush();
            }

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }

        finally{
            try{
                if (inputStream != null){
                    inputStream.close();
                }

                if(outputStream != null){
                    outputStream.close();
                }
                if (socket != null){
                    socket.close();
                }
            }
            catch(IOException ie){
                ie.printStackTrace();
            }
        }
    }

    private void closeCompetition() {
        trans.setCompetitionActive(false);
        simulatorConnector.disconnect();
        startingTS = null;
    }
}