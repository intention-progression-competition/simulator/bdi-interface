// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class BDI {

    static int BDIPortNumber = 40000;

    public static void main(String args[]){

        Socket socket;
        ServerSocket serverSocket = null;

        SimulatorConnector simConn = new SimulatorConnector();

        int i = 0;
        String arg;

        while(i < args.length && args[i].startsWith("-")){
            arg = args[i++];
            if(arg.length() != 2) {
                System.out.println(arg + " is not a valid flag");
                System.exit(1);
            }

            char flag = arg.charAt(1);
            switch (flag) {
                case 'H': // hostname
                    simConn.setSimulatorHostName(args[i++]);
                    break;
                case 'P': // simulator port
                    simConn.setSimulatorPortNumber(Integer.parseInt(args[i++]));
                    break;
                case 'L': // agent listening port
                    BDIPortNumber = Integer.parseInt(args[i++]);
                    break;
                case 'C': // set container sever hostname
					System.out.println("CONTAINERISED SOLUTION MODE ENABLED");
					simConn.setSimulatorHostName("ipc_server");
					break;
            }
        }

        System.out.println("BDI Listening......");

        try{
            serverSocket = new ServerSocket(BDI.BDIPortNumber);
        }
        catch(IOException e){
            e.printStackTrace();
            System.out.println("BDI error");
        }

        while (true) {
            try {
                socket = serverSocket.accept();
                System.out.println("Client connection established");

                ServerThread serverThread = new ServerThread(socket, simConn);
                serverThread.start();
            }

            catch(Exception e){
                e.printStackTrace();
                System.out.println("BDI Connection Error");
                System.exit(1);
            }
        }
    }
}
