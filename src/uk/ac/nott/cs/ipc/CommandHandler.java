// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.StringReader;

/**
 * Created by sgreen on 30/12/2018.
 */
public class CommandHandler {

    CommandTranslator trans;

    public CommandHandler() {
        trans = new CommandTranslator();
    }

    public CommandTranslator parseRequest(String userCommand) throws NullPointerException
    {
        if (userCommand == null) {
            throw new NullPointerException();
        }

        this.parseXML(userCommand);

        if (!trans.isCompetitionActive() && trans.getCommand() != CommandTranslator.Command.INITIALISE) {
            trans.setCommand(CommandTranslator.Command.INVALID_SYNTAX);
        }

        return trans;
    }

    private String parseXML(String command) {
        try {
            InputSource xmlCmd = new InputSource(new StringReader(command.trim()));
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            XMLCommandHandler xmlCmdHandler = new XMLCommandHandler();
            saxParser.parse(xmlCmd, xmlCmdHandler);

        } catch (Exception e) {
            trans.setCommand(CommandTranslator.Command.INVALID_SYNTAX);
        }

        return "";
    }

    class XMLCommandHandler extends DefaultHandler {

        private boolean inSeed = false;
        private boolean inTimeout = false;
        private boolean inGPTFile = false;
        private boolean inAction = false;
        private boolean inQuit = false;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

            if (qName.equalsIgnoreCase("command")) {
                trans.setClientId(attributes.getValue("clientid"));
            }

            if (qName.equalsIgnoreCase("initiate")) {
                trans.setCommand(CommandTranslator.Command.INITIALISE);
                inSeed = true;
            }

            if (qName.equalsIgnoreCase("seed")) {
                inSeed = true;
                trans.setUseSeed(true);
            }

            if (qName.equalsIgnoreCase("timeout")) {
                inTimeout = true;
                trans.setUseTimeout(true);
            }

            if (qName.equalsIgnoreCase("gptfile")) {
                inGPTFile = true;
                trans.setUseGPTFile(true);
            }

            if (qName.equalsIgnoreCase("action")) {
                trans.setCommand(CommandTranslator.Command.ACTION);
                inAction = true;
            }

            if (qName.equalsIgnoreCase("quit")) {
                inQuit = true;
                trans.setCommand(CommandTranslator.Command.TERMINATE);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {

            if (qName.equalsIgnoreCase("seed")) {
                inSeed = false;
            }

            if (qName.equalsIgnoreCase("gptfile")) {
                inGPTFile = false;
            }

            if (qName.equalsIgnoreCase("timeout")) {
                inTimeout = false;
            }

            if (qName.equalsIgnoreCase("quit")) {
                inQuit = false;
            }

            if (qName.equalsIgnoreCase("action")) {
                inAction = false;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (inAction) {
                trans.setAction(new String(ch, start, length));
            }

            if (inSeed) {
                trans.setSeed(Integer.parseInt(new String(ch, start, length)));
            }

            if (inTimeout) {
                trans.setTimeout(Long.parseLong(new String(ch, start, length)));
            }

            if (inGPTFile) {
                trans.setGptFile(new String(ch, start, length));
            }
        }
    }
}