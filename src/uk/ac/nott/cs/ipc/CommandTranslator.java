// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import org.json.JSONObject;

import java.time.Instant;

public class CommandTranslator {

    private String clientId;
    private String instanceId;
    private int seed;
    private boolean useSeed;
    private long timeout;
    private boolean useTimeout;
    private String action;
    private String gptFile;
    private boolean useGPTFile;
    private long tickTime = 0;

    private Command command;
    private boolean competitionActive = true;

    public enum Command {
        INITIALISE, ACTION, TERMINATE, INVALID_SYNTAX
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public boolean isUseSeed() {
        return useSeed;
    }

    public void setUseSeed(boolean useSeed) {
        this.useSeed = useSeed;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public boolean isUseTimeout() {
        return useTimeout;
    }

    public void setUseTimeout(boolean useTimeout) {
        this.useTimeout = useTimeout;
    }

    public String getGptFile() {
        return gptFile;
    }

    public void setGptFile(String gptFile) {
        this.gptFile = gptFile;
    }

    public boolean isUseGPTFile() {
        return useGPTFile;
    }

    public void setUseGPTFile(boolean useGPTFile) {
        this.useGPTFile = useGPTFile;
    }

    public String getSimulatorCommand() {
        return this.prepareSimulatorCommand().toString();
    }

    private JSONObject prepareSimulatorCommand() {

        JSONObject json = new JSONObject();
        json.put("clientid", this.getClientId());

        switch (this.getCommand()) {
            case ACTION:
                json.put("instanceid", this.getInstanceId());
                json.put("command", "action");
                json.put("action", this.getAction());
                json.put("ticktime", this.getTickTime());
                break;

            case INITIALISE:
                this.setCompetitionActive(true);
                json.put("instanceid", this.generateInstanceId());
                json.put("command", "initialise");
                if (this.isUseSeed()) {
                    json.put("seed", this.getSeed());
                }
                if (this.isUseGPTFile()){
                    json.put("gptfile", this.getGptFile());
                }
                if (this.isUseTimeout()){
                    json.put("timeout", this.getTimeout());
                }
                break;

            case TERMINATE:
                json.put("instanceid", this.getInstanceId());
                json.put("command", "quit");
                this.setCompetitionActive(false);

                break;
        }
        
        return json;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public String generateInstanceId() {
        this.instanceId = String.valueOf(Instant.now().toEpochMilli());

        return this.getInstanceId();
    }

    public long getTickTime() {
        return tickTime;
    }

    public void setTickTime(long tickTime) {
        this.tickTime = tickTime;
    }

    public boolean isCompetitionActive() {
        return competitionActive;
    }

    public void setCompetitionActive(boolean competitionActive) {
        this.competitionActive = competitionActive;
    }
}
