// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class SimulatorConnector {
    
    private String simulatorHostName = "127.0.0.1";
    private int simulatorPortNumber = 30000;

    private Socket socket;
    private PrintWriter serverOut;
    private BufferedReader serverIn;
    private boolean competitionTerminated = false;

    public SimulatorConnector() {}

    public void connect() {

        //TODO - test if socket already open and do not reopen if it is.
        try {
            socket = new Socket(this.simulatorHostName, this.simulatorPortNumber);
            serverOut = new PrintWriter(socket.getOutputStream(), true);
            serverIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            System.out.println("Connection to Simulator established on port " + this.simulatorPortNumber);

        } catch(UnknownHostException e) {
            e.printStackTrace();
        } catch(IOException e) {
            System.out.println("IO Exception when outbound connection requested to " + this.getSimulatorHostName() + " on port " + this.getSimulatorPortNumber());
            e.printStackTrace();
        }
    }

    public void disconnect() {
        if (this.isConnected()) {
            try {
                socket.close();
                System.out.println("Connection to Simulator closed");
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isConnected() {
        return socket != null && socket.isConnected();
    }

    public void sendMessage(String userCommand) {
        if (!isConnected()) {
            connect();
        }
        serverOut.println(userCommand);
    }

    public String getSimulatorResponseXML() {

        String simResponseXML;

        //get response from server
        try {
            String simulatorResponse = serverIn.readLine();
            JSONObject simResponseJSON = new JSONObject(simulatorResponse);

            if (simResponseJSON.has("gptcontents")) {

                String gptFileName = simResponseJSON.getJSONArray("gptfile").getString(0);
                JSONArray fileJsonContents = simResponseJSON.getJSONArray("gptcontents");

                this.writeToFile(gptFileName, fileJsonContents);
                simResponseJSON.remove("gptcontents");
            }

            if (simResponseJSON.has("logcontents")) {

                String logFileName = simResponseJSON.getJSONArray("logfile").getString(0);
                JSONArray fileJsonContents = simResponseJSON.getJSONArray("logcontents");

                this.writeToFile(logFileName, fileJsonContents);
                simResponseJSON.remove("logcontents");
            }

            if (simResponseJSON.has("status")) {
                JSONObject contestStatusJSON = simResponseJSON.getJSONArray("status").getJSONObject(0);

                if (contestStatusJSON.has("contest")) {
                    String contestState = contestStatusJSON.getJSONArray("contest").getString(0);
                    
                    if (contestState.equals("COMPLETE") || contestState.equals("TIMEOUT")) {
                        this.setCompetitionTerminated(true);
                    }

                    if (contestState.equals("ACTIVE")) {
                        this.setCompetitionTerminated(false);
                    }
                }
            }

            simResponseXML = XML.toString(simResponseJSON, "msgroot");

        } catch(IOException e) {
            e.printStackTrace();
            simResponseXML = "<error>Invalid Response detected</error>"; //TODO fix this
        }

        return simResponseXML;
    }

    private void writeToFile(String fileName, JSONArray gptJSON) {
        String fileContents = gptJSON.getString(0);
        File file = new File(fileName);
        file.getParentFile().mkdirs();

        try {
            PrintWriter gptFile = new PrintWriter(fileName);
            gptFile.println(fileContents);
            gptFile.close();

        } catch(FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getSimulatorHostName() {
        return simulatorHostName;
    }

    public void setSimulatorHostName(String simulatorHostName) {
        this.simulatorHostName = simulatorHostName;
    }

    public int getSimulatorPortNumber() {
        return simulatorPortNumber;
    }

    public void setSimulatorPortNumber(int simulatorPortNumber) {
        this.simulatorPortNumber = simulatorPortNumber;
    }

    public boolean isCompetitionTerminated() {
        return competitionTerminated;
    }

    public void setCompetitionTerminated(boolean competitionTerminated) {
        this.competitionTerminated = competitionTerminated;
    }
}
