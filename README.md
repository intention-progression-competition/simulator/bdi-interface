The BDI interface is designed to sit between an entrant's IPP Sover and the Simulator Server. It is installed locally on the entrant's VM and listens on port 40000 for connections from the IPP Solver.

On receipt of a connection from the IPP Solver, it awaits commands from the solver and then initiates a connection to the server on port 30000 (the default port numbers can be changed using command line options).  The BDI Interface accepts commands in XML format (as detailed below), appends some additional information (such as timestamps, instance ids, etc) and then forwards the completed packet to the server in JSON format. 

Examples of commands expected from a client are as follows: 

***Initiation***

An IPP instance or session is defined as a single attempt to complete a competition problem.

Initiate an IPP instance:
```
<?xml version="1.0" encoding="UTF-8"?>
<command clientid="EXAMPLEID">
    <initiate>
        <seed>10000</seed>
	<gptfile>gpts/example-gpt.xml</gptfile>
    </initiate>
</command>
```
The initiation request takes a number of parameters related to the instance. These include:

*  *seed* - allows the client to specify the seed to be used for the attempt (to allow reproducibility). Will be ignored when run under test conditions.

*  *gptfile* - specifies the IPP instance they wish to attempt. The file containing the forest of goal-plan trees for the specified instance is returned by the Simulation Server and saved in a local file (with the same path relative to the BDI Interface) for use by the entrant's IPP Solver.


***Action***

Request an action be performed:
```
<?xml version="1.0" encoding="UTF-8"?>
<command clientid="EXAMPLEID">
    <action>T3-A155</action>
</command>
```

***Quit***

Terminate the current session:
```
<?xml version="1.0" encoding="UTF-8"?>
<command clientid="EXAMPLEID">
    <quit></quit>
</command>
```

On receipt of a valid JSON response from the simulator, the BDI Interface converts to XML and forwards on to the agent for processing accordingly.


***Launch***

After downloading, in order to build and run the BDI interface, create the Jar file using a gradle build process: 

`./gradlew fatJar`

Then launch the BDI Interface using: 

`java -jar build/libs/bdi-interface.jar 

Command line options allow the host:port to be specified to avoid port collisions in your local environment. 

The -H parameter specifies the host running the Simulator Server (the default is 127.0.0.1). When running on your VM, the competition server can be specified using -H contest00.cs.nott.ac.uk or -H contest00 

The -P parameter specifies the port to use when connecting to the Simulator Server (the default is 30000). 

The -L parameter specifies the port to bind when listening for the entrat's IPP Solver (the default is 40000).




